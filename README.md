# How to update the packages

## Updating stable
Whenever there's a release for km-app master, there should be an update to the stable branch of this repo.

1. To update, first clone the repository. 
2. Next, open PKGBUILD and change the pkgver and the url for km-app so that it points to the latest tag. 
3. (optional) Then, you should add / remove stuff as needed (ex: electron 30 was broken on Wayland for a bit, so we had to depend on electron29 for a while). 
4. Next step is running `updpkgsums` to compute the new checksums. The computed checksums get appended at the end of the file so you should take the appended checksums and replace the old ones.
5. Finally, try to build the package by running `makepkg PKGBUILD`. If the build is successful, you can commit your changes and the pipeline should push your changes to the AUR. The pipeline does not run automatically so you **have** to start it manually. This is so that we only ship the necessary to the AUR.

If you want to make changes to the packaging w/o changing version, you only need to increment the `pkgrel`. Remember to put it back to 1 in the next pkgver update.

## Updating master
Master is the branch used for the karaokemugen-git package. Since it is a git package, you do not need to change it's pkgver or the karaokemugen-app git url. If you are not making packaging related changes, just run `makepkg PKGBUILD` and the new version will be computed automagically. Then commit changes as usual.

It is not necessary nor recommended to update master for each commit. Users who want the latest version can always rebuild the package whenever to get the latest commits.